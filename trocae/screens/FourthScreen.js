import React from 'react';
import { Text, StyleSheet, View } from 'react-native';

export default function FourthScreen() {
  return (
    <View style={styles.container}>
      <Text>Fourth Screen</Text>
    </View>
  );
}

FourthScreen.navigationOptions = {
  title: 'Fourth',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
});
