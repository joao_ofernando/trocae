import React from 'react';
import { Text, StyleSheet, View } from 'react-native';


export default function SecondScreen() {
  return (
    <View style={styles.container}>
      <Text>Second Screen</Text>
    </View>
  );
}

SecondScreen.navigationOptions = {
  title: 'Second',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
});
