import React from 'react';
import {
    Text,
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
} from 'react-native';

screenWidth = Dimensions.get('window').width;

import LoginInput from '../components/LoginInput';
import TouchButton from '../components/TouchButton';

import logoImg from '../assets/images/logo.png'
import userImg from '../assets/images/user.png'
import lockImg from '../assets/images/lock.png'
import topImg from '../assets/images/topImg.png'

import Colors from '../constants/Colors';
import Font from '../constants/Font';


export default class LoginScreen extends React.Component {

    static navigationOptions = {
        header: null
    }

    render() {
        return (
            <>
                <StatusBar barStyle="light-content" backgroundColor="blue" backgroundColor="#772ea2" />
                <View style={styles.rootContainer}>
                    <KeyboardAvoidingView behavior="padding" style={styles.KeyboardAvoidingView}>
                        <View style={styles.imageTop}>
                            <Image source={topImg} />
                        </View>

                        <View style={styles.formContainer}>
                            <View style={styles.logoContainer}>
                                <Image source={logoImg} style={styles.logo} />
                            </View>
                            <LoginInput
                                image={userImg}
                                placeHolder={'usuário'}
                                security={false}
                            />
                            <LoginInput
                                image={lockImg}
                                placeHolder={'senha'}
                                security={true}
                            />
                            <View style={styles.buttonContainer}>
                                <TouchButton
                                    text={'entrar'}
                                    route={"Home"}
                                    navigation={this.props.navigation}
                                />
                            </View>
                            <TouchableOpacity>
                                <Text style={styles.forgottenText}>esqueceu sua senha?</Text>
                            </TouchableOpacity>
                        </View>
                    </KeyboardAvoidingView>
                    <TouchableOpacity style={styles.textCreatAccountContainer}>
                        <Text style={styles.textCreatAccount}>criar uma conta</Text>
                    </TouchableOpacity>
                </View>
            </>
        );
    }
}

const styles = StyleSheet.create({
    rootContainer: {
        flex: 1,
        backgroundColor: '#fff',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    formContainer: {
        flex: 1,
        backgroundColor: '#fff',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    logoContainer: {
        padding: 5,
    },
    logo: {
        width: 178,
        height: 56,
        marginBottom: 67,
    },
    forgottenText: {
        fontSize: 12,
        color: '#A4A4A4',
        fontFamily: Font.regular,
    },
    textCreatAccount: {
        fontSize: 13,
        color: Colors.standard,
        fontFamily: Font.bold,
    },
    textCreatAccountContainer: {
        marginBottom: 25,
    },
    imageTop: {
        width: '100%',
    },
    KeyboardAvoidingView: {
        flex: 1,
    },
    buttonContainer: {
        width: screenWidth - 55,
    },

})