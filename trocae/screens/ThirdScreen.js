import React from 'react';
import { Text, StyleSheet, View } from 'react-native';

export default function ThirdScreen() {
  return (
    <View style={styles.container}>
      <Text>Third Screen</Text>
    </View>
  );
}

ThirdScreen.navigationOptions = {
  title: 'Third',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
});
