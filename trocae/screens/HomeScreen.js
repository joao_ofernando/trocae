import React from 'react';
import {
  Image,
  StyleSheet,
  View,
  Text,
  ScrollView,
  Modal,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { BlurView } from 'expo-blur';

screenWidth = Dimensions.get('window').width;

import ListScrollItem from '../components/ListScrollItem'
import SearchBar from '../components/SearchBar';
import ItemList from '../components/ItemList';
import TouchButton from '../components/TouchButton';

import logoImg from '../assets/images/logoMin.png';
import menuIcon from '../assets/images/menuIcon.png';
import clothesImg from '../assets/images/clothesImg.png'
import beautyImg from '../assets/images/beautyImg.png'
import gamesImg from '../assets/images/gamesImg.png'
import barImg from '../assets/images/barImg.png'
import childishImg from '../assets/images/childishImg.png'
import eletronicImg from '../assets/images/eletronicImg.png'
import tripImg from '../assets/images/tripImg.png'
import restaurantImg from '../assets/images/restaurantImg.png'
import locateImg from '../assets/images/locateImg.png'
import UserImg from '../assets/images/user.png'
import TradeImg from '../assets/images/tradeImg.png'
import closeIcon from '../assets/images/closeIcon.png'

import Font from '../constants/Font';
import Colors from '../constants/Colors';

const items = [
  {
    product: "SmartTV 4K Philips 50''",
    locate: "São Pauto - SP",
    image: require('../assets/images/imgUm.png'),
    description: 'Troco Canon EOS 60D com 12.000 clicks, 1 ano e meio de uso, com nota fiscal, garantia de 6 meses com possível renovação. Acompanha bateria, carregador, lente Youngnuo 18-35mm e suporte para pescoço. Está em excelente estado de conservação, praticamente nova.',
    distance: '12km',
    advertiser: 'joao',
    tradeFor: 'carro',
  },
  {
    product: "CANON EOS 60D - 12.000k",
    locate: "São Pauto - SP",
    image: require('../assets/images/imgDois.png'),
    description: 'Troco Canon EOS 60D com 12.000 clicks, 1 ano e meio de uso, com nota fiscal, garantia de 6 meses com possível renovação. Acompanha bateria, carregador, lente Youngnuo 18-35mm e suporte para pescoço. Está em excelente estado de conservação, praticamente nova.',
    distance: '12km',
    advertiser: "Márcia Sana' anna",
    tradeFor: 'iPhone XS Max - 128GB novo ou seminovo',
  },
  {
    product: "MacBook Air i5-8400 13'v",
    locate: "São Pauto - SP",
    image: require('../assets/images/imgTres.png'),
    description: 'Troco Canon EOS 60D com 12.000 clicks, 1 ano e meio de uso, com nota fiscal, garantia de 6 meses com possível renovação. Acompanha bateria, carregador, lente Youngnuo 18-35mm e suporte para pescoço. Está em excelente estado de conservação, praticamente nova.',
    distance: '12km',
    advertiser: 'joao',
    tradeFor: 'carro',
  },
  {
    product: "Kit Shutz ROLLING TRIANGLE",
    locate: "São Pauto - SP",
    image: require('../assets/images/imgQuatro.png'),
    description: 'Troco Canon EOS 60D com 12.000 clicks, 1 ano e meio de uso, com nota fiscal, garantia de 6 meses com possível renovação. Acompanha bateria, carregador, lente Youngnuo 18-35mm e suporte para pescoço. Está em excelente estado de conservação, praticamente nova.',
    distance: '12km',
    advertiser: 'joao',
    tradeFor: 'carro',
  },
  {
    product: "Smartphone Samsung Galaxy S10",
    locate: "São Pauto - SP",
    image: require('../assets/images/imgCinco.png'),
    description: 'smartphone',
    distance: '12km',
    advertiser: 'joao',
    tradeFor: 'carro',
  },
  {
    product: "Cartucho HP 21XL preto Original",
    locate: "São Pauto - SP",
    image: require('../assets/images/imgSeis.png'),
    description: 'cartucho',
    distance: '12km',
    advertiser: 'joao',
    tradeFor: 'carro',
  },
];

export default class HomeScreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      items,
      isVisible: false,
      active: {}
    }
  };

  handleModal = async (item) => {
    this.setState({ active: item })
    this.handleOpenCloseModal()
  }
  handleOpenCloseModal() {
    const { isVisible } = this.state;
    this.setState({ isVisible: !isVisible })
  }

  render() {
    const { isVisible, active } = this.state;
    const { product, locate, image, description, distance, advertiser, tradeFor } = active;

    return (
      <View style={styles.container}>
        {active &&
          <Modal
            animationType="slide"
            transparent={true}
            visible={isVisible}
          >
            <BlurView tint="dark" intensity={85} style={styles.blurView}>
              <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.rootModal}>
                  <View style={styles.modalContainer}>
                    <TouchableOpacity
                      style={styles.closeIcon}
                      onPress={() => {
                        this.handleOpenCloseModal()
                      }}>
                      <Image source={closeIcon} />
                    </TouchableOpacity>
                    <Image source={image} style={styles.modalImage} />
                  </View>
                  <View style={styles.descriptionContainer}>
                    <View style={styles.locateContainer}>
                      <Image source={locateImg} style={styles.locateImage} />
                      <Text style={styles.locateText}>{distance}</Text>
                      <Text style={styles.locateText}>{locate}</Text>
                    </View>
                    <Text style={styles.productText}>{product}</Text>
                    <Text style={styles.descriptionText}>{description}</Text>
                    <View style={styles.infoContainer}>
                      <Image source={UserImg} style={styles.labelImage} />
                      <Text style={styles.labelInfo}>anunciante: </Text>
                      <Text style={styles.contentText}>{advertiser}</Text>
                    </View>
                    <View style={styles.infoContainer}>
                      <Image source={TradeImg} style={styles.labelImage} />
                      <Text>
                        <Text style={styles.labelInfo}>troca por: </Text>
                        <Text style={styles.contentText}>{tradeFor}</Text>
                      </Text>
                    </View>

                  </View>
                  <View style={styles.buttonContainer}>
                    <TouchButton text={'trocaê'} route={""} navigation={this.props.navigation} />
                  </View>
                </View>
              </ScrollView>
            </BlurView>
          </Modal>
        }
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.logoContainer}>
            <Image source={logoImg} style={styles.logoImg} />
          </View>
          <View style={styles.topContainer}>
            <View>
              <TouchableOpacity>
                <Image source={menuIcon} style={styles.menuIcon} />
              </TouchableOpacity>
            </View>
            <SearchBar />
          </View>
          <View>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={styles.menuList}>
              <ListScrollItem image={clothesImg} />
              <ListScrollItem image={beautyImg} />
              <ListScrollItem image={gamesImg} />
              <ListScrollItem image={barImg} />
              <ListScrollItem image={childishImg} />
              <ListScrollItem image={eletronicImg} />
              <ListScrollItem image={tripImg} />
              <ListScrollItem image={restaurantImg} />
              <ListScrollItem image={eletronicImg} />
            </ScrollView>
          </View>
          <View style={styles.textFeaturedContainer}>
            <Text style={styles.textFeatured}>Destaques</Text>
          </View>
          <View style={styles.listItem}>
            <ItemList
              items={this.state.items}
              handleModal={(e) => this.handleModal(e)}
            />
          </View>
        </ScrollView>
      </View>

    );
  }
}

HomeScreen.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    fontFamily: Font.regular,
  },
  logoContainer: {
    marginTop: 38,
    alignItems: 'center',
  },
  logoImg: {
    height: 36,
    width: 44,
  },
  menuIcon: {
    marginLeft: 6,
    height: 25,
    width: 26,
  },
  topContainer: {
    marginTop: 31,
    flexDirection: 'row',
    alignItems: 'center',
  },
  menuList: {
    marginTop: 24,
    height: 59,
    paddingHorizontal: 20,
  },
  textFeaturedContainer: {
    marginTop: 18,
  },
  textFeatured: {
    fontSize: 14,
    marginLeft: 25,
    color: Colors.standard,
    fontFamily: Font.bold,
  },
  listItem: {
    alignItems: 'center'
  },
  modalImage: {
    width: screenWidth - 55,
    height: 260,
    margin: 9,
    borderRadius: 15,
  },
  modalContainer: {
    alignItems: 'center',
    flex: 1,
  },
  locateContainer: {
    marginTop: 8,
    flexDirection: 'row',
    alignItems: 'center',
  },
  locateImage: {
    width: 11,
    height: 13,
  },
  locateText: {
    marginLeft: 7,
    marginRight: 12,
    fontSize: 11,
    fontFamily: Font.regular,
  },
  productText: {
    marginTop: 5,
    marginBottom: 2,
    fontSize: 14,
    fontFamily: Font.bold,
  },
  descriptionContainer: {
    marginHorizontal: 20,
  },
  descriptionText: {
    fontSize: 11,
    fontFamily: Font.regular,
    textAlign: 'left',
  },
  labelImage: {
    height: 16,
    width: 15,
    marginRight: 4,
  },
  labelInfo: {
    fontSize: 13,
    fontFamily: Font.bold
  },
  contentText: {
    fontSize: 13,
    flex: 1,
    flexGrow: 1,
    fontFamily: Font.regular,
  },
  infoContainer: {
    marginTop: 6,
    flexDirection: 'row',
  },
  buttonContainer: {
    marginHorizontal: 20,
    marginTop: 18,
    marginBottom: 15,
    justifyContent: 'center',
  },
  closeIcon: {
    zIndex: 2,
    left: '85%',
    top: '5%',
    position: 'absolute',
  },
  blurView: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
  rootModal: {
    margin: 20,
    borderRadius: 20,
    backgroundColor: 'white',
  }
});
