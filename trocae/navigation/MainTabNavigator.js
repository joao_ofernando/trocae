import React from 'react';
import { Platform, Image } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import HomeScreen from '../screens/HomeScreen';
import SecondScreen from '../screens/SecondScreen';
import ThirdScreen from '../screens/ThirdScreen';
import FourthScreen from '../screens/FourthScreen';

import homeIcon from '../assets/images/homeIcon.png';
import megaphoneIcon from '../assets/images/megaphoneIcon.png';
import mapIcon from '../assets/images/mapIcon.png';
import chatIcon from '../assets/images/chatIcon.png';

const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},
});

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
  },
  config
);

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (
    <Image
      source={homeIcon}
      style={{ width: 25, height: 25, paddingTop: 0 }}
      opacity={focused ? 1.0 : 0.3}
    />),
};

HomeStack.path = '';

const SecondStack = createStackNavigator(
  {
    Second: SecondScreen,
  },
  config
);

SecondStack.navigationOptions = {
  tabBarLabel: 'Second',
  tabBarIcon: ({ focused }) => (
    <Image
      source={megaphoneIcon}
      style={{ width: 32, height: 25, paddingTop: 0 }}
      opacity={focused ? 1.0 : 0.3}
    />),
};

SecondStack.path = '';

const ThirdStack = createStackNavigator(
  {
    Third: ThirdScreen,
  },
  config
);

ThirdStack.navigationOptions = {
  tabBarLabel: 'Third',
  tabBarIcon: ({ focused }) => (
    <Image
      source={mapIcon}
      style={{ width: 30, height: 25, paddingTop: 0 }}
      opacity={focused ? 1.0 : 0.3}
    />),
};

ThirdStack.path = '';


const FourthStack = createStackNavigator(
  {
    Fourth: FourthScreen,
  },
  config
);

FourthStack.navigationOptions = {
  tabBarLabel: 'Fourth',
  tabBarIcon: ({ focused }) => (
    <Image
      source={chatIcon}
      style={{ width: 25, height: 25, paddingTop: 0 }}
      opacity={focused ? 1.0 : 0.3}
    />),
};

FourthStack.path = '';


const tabNavigator = createBottomTabNavigator({
  HomeStack,
  SecondStack,
  ThirdStack,
  FourthStack
},
  {
    tabBarOptions: {
      showLabel: false,
      style: {
        height: 58,
        backgroundColor: '#F17D08'
      }
    }
  });

tabNavigator.path = '';

export default tabNavigator;
