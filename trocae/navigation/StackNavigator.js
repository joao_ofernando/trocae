import { createStackNavigator } from 'react-navigation';

import LoginScreen from '../screens/LoginScreen';


const LoginStack = createStackNavigator(
  {
    Login: LoginScreen,
  }
);

LoginStack.navigationOptions = {
  header: null
};


export default LoginStack;
