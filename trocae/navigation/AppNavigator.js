import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import LoginStack from './StackNavigator';

export default createAppContainer(
  createSwitchNavigator({
    Login: LoginStack,
    Main: MainTabNavigator,
  })
);
