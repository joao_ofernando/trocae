
import React from 'react';
import { View, StyleSheet, TouchableOpacity, Image, TextInput } from 'react-native';

import searchIcon from '../assets/images/searchIcon.png';
import Font from '../constants/Font';

const SearchBar = (props) => {

    return (
        <View style={styles.searchInputContainer}>
            <View style={styles.inputContainer}>
                <TextInput style={styles.searchInput} placeholder={'por qual item você quer trocar?'} />
            </View>
            <TouchableOpacity>
                <Image source={searchIcon} style={styles.searchIcon} />
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    searchInputContainer: {
        flex: 1,
        marginRight: 16,
        marginLeft: 17,
        borderRadius: 50,
        borderWidth: 1,
        borderColor: 'black',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    searchInput: {
        flex: 1,
        paddingVertical: 12,
        marginLeft: 17,
        fontSize: 11,
        alignItems: 'stretch',
        fontFamily: Font.regular,
    },
    searchIcon: {
        marginRight: 15,
        width: 20,
        height: 20,
    },
    inputContainer: {
        flex: 1,
    }
});

export default SearchBar;