
import React from 'react';
import { StyleSheet, TouchableOpacity, Image } from 'react-native';

const ListScrollItem = (props) => {
    const { image } = props;

    return (
        <TouchableOpacity>
            <Image source={image} style={styles.image} />
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    image: {
        marginRight: 7,
        height: 58,
        width: 58,
    }
});

export default ListScrollItem;