
import React from 'react';
import { StyleSheet, TouchableOpacity, Text } from 'react-native';
import Font from '../constants/Font';


const LoginInput = (props) => {
    const { text, route } = props;

    return (
        <TouchableOpacity
            style={styles.button}
            onPress={() => props.navigation.navigate(route)}
        >
            <Text style={styles.textButton}>{text}</Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    button: {
        elevation: 1,
        borderRadius: 50,
        height: 40,
        marginTop: 2,
        marginBottom: 10,
        backgroundColor: '#F17D08',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textButton: {
        color: 'white',
        fontFamily: Font.regular,
    },
});

export default LoginInput;