
import React from 'react';
import { View, StyleSheet, Dimensions, Image, TextInput } from 'react-native';

import Font from '../constants/Font'
import Colors from '../constants/Colors'

const { width: WIDTH } = Dimensions.get('window');

const LoginInput = (props) => {
    const { placeHolder, image, security } = props;

    return (
        <View style={styles.container}>
            <Image source={image} style={styles.imgInput} />
            <TextInput
                style={security ? styles.password : styles.textInput}
                placeholder={placeHolder}
                secureTextEntry={security}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        borderWidth: 1,
        borderRadius: 50,
        paddingHorizontal: 19,
        paddingTop: 12,
        paddingBottom: 13,
        width: WIDTH - 54,
        marginBottom: 27,
        alignItems: 'center',
        borderColor: 'black',
        flexDirection: 'row',
    },
    imgInput: {
        width: 15,
        height: 18,
        marginRight: 14,
    },
    textInput: {
        flex: 1,
        fontSize: 16,
        color: '#A4A4A4',
        fontFamily: Font.regular,
    },
    password: {
        flex: 1,
        fontSize: 16,
        color: Colors.standard,
        fontFamily: Font.regular,
    },
});

export default LoginInput;