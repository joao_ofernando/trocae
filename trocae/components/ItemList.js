import React from 'react';
import { FlatList, StyleSheet, View, TouchableOpacity, Image, Text } from 'react-native';

import Font from '../constants/Font';
import locateImg from '../assets/images/locateImg.png'


const ItemList = props => {
    const { items, handleModal } = props;

    return (
        <FlatList
            contentContainerStyle={styles.container}
            showsVerticalScrollIndicator={false}
            data={items}
            numColumns={2}
            renderItem={({ item }) => (
                <TouchableOpacity style={styles.cardContainer} onPress={
                    () => handleModal(item)
                }>
                    <Image source={item.image} style={styles.image} />
                    <View style={styles.locateContainer}>
                        <Image source={locateImg} style={styles.locateImage} />
                        <Text style={styles.locateText}>{item.locate}</Text>
                    </View>
                    <Text style={styles.productText}>{item.product}</Text>
                </TouchableOpacity>
            )}
            keyExtractor={(item, index) => item.product + index}
        />
    )
}

const styles = StyleSheet.create({
    container: {
        paddingBottom: 20,
        marginTop: 15,
        alignItems: 'flex-start',
        justifyContent: 'space-around'
    },
    cardContainer: {
        flexGrow: 1,
        margin: 10,
        maxWidth: 140,
        paddingTop: 8,
        paddingHorizontal: 4,
        borderRadius: 10,
        shadowColor: "#000",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        backgroundColor: 'white',
    },
    image: {
        borderRadius: 10,
        height: 130,
        width: 130
    },
    locateContainer: {
        marginTop: 8,
        marginLeft: 12,
        flexDirection: 'row',
        alignItems: 'center',
    },
    locateImage: {
        width: 8,
        height: 9,
    },
    locateText: {
        marginLeft: 7,
        fontSize: 9.5,
        fontFamily: Font.regular,
    },
    productText: {
        marginLeft: 12,
        marginTop: 7,
        marginBottom: 15,
        fontSize: 9.5,
        fontFamily: Font.bold,
    }
});

export default ItemList;